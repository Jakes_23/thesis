    #ifndef sensor_commands_H
    #define sensor_commands_H

    #include <Arduino.h>
    #include <SoftwareSerial.h>
    #include <DHT.h>

    //Pin Declaration
    #define DHTPIN 12

    //DHT11 type
    #define DHT_TYPE DHT11
    #define verbose_mode2 false

    class sensor_commands{
    public:
      sensor_commands();

      //Read functions
			float getTemperature();
      float getHumidity();
    };

    #endif
