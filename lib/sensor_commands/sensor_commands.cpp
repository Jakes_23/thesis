#include <sensor_commands.h>

DHT dht(DHTPIN, DHT_TYPE);

//Constructor function ---------------------------------------------------------------------------------
sensor_commands::sensor_commands()
{
  dht.begin();
}

//Reads the temperature and checks if value
float sensor_commands::getTemperature()
{
  float t = dht.readTemperature();

  if(isnan(t))
  {
    if(verbose_mode2) Serial.println("[ERROR] Temperature reading failed");
    t=999;
  }
  else if(verbose_mode2)
  {
    Serial.print("DHT11 Temp: ");
    Serial.print(t);
    Serial.println(" *C >>");
  }
  return t;

}


float sensor_commands::getHumidity()
{
  float h = dht.readHumidity();

  if(isnan(h))
  {
    if(verbose_mode2) Serial.println("[ERROR] Humidity reading failed");
    h=999;
  }
  else if(verbose_mode2)
  {
    Serial.print("DHT11 Humidity: ");
    Serial.print(h);
    Serial.println(" % >>");
  }
  return h;
}
