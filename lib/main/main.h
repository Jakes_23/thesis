#ifndef main_H
#define main_H

#include <Arduino.h>
#include <avr/sleep.h>
#include <avr/power.h>
#include <avr/wdt.h>
#include <time.h>


//Choose system's mode
#define MODE DEBUGGING_MODE
//Define modes
#define OPERATIONAL_MODE 0
#define DEBUGGING_MODE 1
#define VERBOSE_MODE 2
//device ID (Slave2)

#define myID 2

static char robinIDs[1] = {'2'};//IDs of other nodes to be polled [ Stored in Gateway ]



#define _gateway_ 0
#define _level1_ 1
#define _level2_ 2
#define _level3_ 3
#define _level4_ 4
#define _level5_ 5
#define _noLevel_ 999

//What is my level?


#endif
