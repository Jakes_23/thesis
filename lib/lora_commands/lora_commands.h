#ifndef lora_commands_H
#define lora_commands_H

#include <main.h>


//Pin Declaration
#define LrxPin 10
#define LtxPin 9

#define _time_out_ 10 //seconds
#define zero_time 0;
//boolean recentTransmission_flag = false;

//char cr = 0xD;//Carriage Return Character
//char lf = 0xA;//Line Feed Character

class lora_commands{
public:
  //Basic Operationas
  lora_commands();
  boolean wakeUp();
  void sleep();
  void sleep10();
  boolean checkStatus();
  boolean makeReady();
  //Data Processing
  boolean strContains(String response, String subString, int startI);
  String string2HEXstr(String normalStr);
  //Transmission Functions

  //Receive Functions
  String readHex2Str(String hexstr);
  void issueCommand(char* command);
  void readSerial2PC();
  String readSerial2Str();
  void printByte(unsigned char c);
  void printString(const char *s);
  String printHEX(String buffer);
  void transmitData(int tempC);
  void packetBursting();
  void setPOWER(int power);
  void newLocation();

  //Functions
  void robin_getDataAsGateway();
  void robin_setTimeToSend(int temp);
  void robin_waitForMyCMD(int temp);

  //P2P Networking
  boolean p2p_isTimeToTransmit();
  boolean p2p_isMyLevelSet();
  void p2p_sendSM();
  void p2p_getSM();
  double p2p_getSNR();
};

#endif
