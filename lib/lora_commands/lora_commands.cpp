#include <lora_commands.h>
#include <SoftwareSerial.h>

int myLevel = _noLevel_;
SoftwareSerial LoRa_Ser(LrxPin,LtxPin);


////////////////////////////////////////////////////////////////////////////////
//                    B A S I C   O P E R A T I O N S                         //
////////////////////////////////////////////////////////////////////////////////

//Initialisation Constructor----------------------------------------------------
lora_commands::lora_commands()
{
}
void lora_commands::sleep()
{
  LoRa_Ser.end();
}

void lora_commands::sleep10()
{
  printString("sys sleep 10000");
  printString("\r\n");
  delay(300);
  readSerial2PC();
  LoRa_Ser.end();
}
boolean lora_commands::wakeUp()
{
  LoRa_Ser.begin(57600);
  LoRa_Ser.flush();
  issueCommand("sys get ver");
  delay(250);
  String response = readSerial2Str();
  return checkStatus();
}
boolean lora_commands::makeReady()
{
  boolean checks = false;
  issueCommand("mac pause");
  delay(250);
  String response = readSerial2Str();
  if(response.equals("4294967245\r\n"))
  {
    checks=true;
    if(MODE==DEBUGGING_MODE) Serial.println("MAC paused >>");
    else if(MODE==VERBOSE_MODE) Serial.println(response);
  }
  else
  {
    if(MODE==DEBUGGING_MODE) Serial.println("[ERROR] MAC pause failed >>");
    else if(MODE==VERBOSE_MODE) Serial.println(response);
  }
  return checks;
}
boolean lora_commands::checkStatus()
{
  boolean checks = false;
  //LoRa_Ser.flush();
  //delay(5);
  //readSerial2PC();
  printString("sys get ver");
  printString("\r\n");
  delay(300);
  String response = readSerial2Str();
  if (MODE) Serial.println("Sys get ver response is:"+response);
  //Serial.println("Needs RN2903:"+response);
  if(strContains(response, "RN2903", 0))
  {
    checks=true;
    if(MODE==DEBUGGING_MODE) Serial.println("LoRa AWAKE >>");
    else if(MODE==VERBOSE_MODE) Serial.println(response);
  }
  else
  {
    if(MODE==DEBUGGING_MODE) Serial.println("[ERROR] LoRa Serial Failed >>");
    else if(MODE==VERBOSE_MODE) Serial.println(response);
  }
  return checks;
}
////////////////////////////////////////////////////////////////////////////////
//                    D A T A   P R O C E S S I N G                           //
////////////////////////////////////////////////////////////////////////////////
//Reads HEX up to '.' character and returns an ASCII text String----------------
String lora_commands::string2HEXstr(String normalStr)
{
  String newString = "";
  int N = normalStr.length()+1;
  char temp[N];
  normalStr.toCharArray(temp, N);
  for(int i=0; i < N-1; i++) newString+=String(temp[i], HEX);
  return newString;
}


boolean lora_commands::strContains(String response, String subString, int startI)
{
  boolean contains = false;
  int n = subString.length();
  int matched = 0;
  for (int i = startI; i< response.length(); i++)
  {
    if(matched<n)
    {
      if(response.charAt(i)==subString.charAt(matched))
      {
        matched+=1;
      }
    }
  }
  if(matched == n) contains = true;
  return contains;
}


////////////////////////////////////////////////////////////////////////////////
//               T R A N S M I S S I O N   F U N C T I O N S                  //
////////////////////////////////////////////////////////////////////////////////
void lora_commands::transmitData(int tempC)
{
  if(MODE) Serial.print("TX>>");
  printString("radio tx ");
  printHEX(String(myID) + " ");
  printHEX(String(tempC) + ".");
  printString("\r\n");
}

//TESTING range

void lora_commands::setPOWER(int power)
{
  printString("radio set pwr 4");
  printString("\r\n");
  delay(300);
  String response = readSerial2Str();
  Serial.println(response);
}
//Prints a command to the LoRa mote---------------------------------------------
void lora_commands::issueCommand(char* command)
{
  Serial.flush();
  LoRa_Ser.flush();
  printString(command);//The command itself
  printString("\r\n");//<CR><LF>
  delay(10);
}
//Receives a String and prints HEX representation to LoRa mote------------------
String lora_commands::printHEX(String buffer)
{
  //LoRa_Ser.flush();
  int length = buffer.length();
  char temp;
  for (int i =0; i< length; i++)
  {
    temp=buffer.charAt(i);
    LoRa_Ser.print(temp, HEX);
    //if(MODE) Serial.print(temp, HEX);
  }
}
//Print STRING one byte at a time to LoRa Mote----------------------------------
void lora_commands::printString(const char *s)
{
  while (*s) printByte(*s++);
}
//Print one BYTE to LoRa mote---------------------------------------------------
void lora_commands::printByte(unsigned char c)
{
  LoRa_Ser.write(c);
}
////////////////////////////////////////////////////////////////////////////////
//                    R E C E I V E   C O M M A N D S                         //
////////////////////////////////////////////////////////////////////////////////

//Read serial input and output it directly to PC -------------------------------
void lora_commands::readSerial2PC()
{
  while (LoRa_Ser.available())
  {
    Serial.write(LoRa_Ser.read());
    delay(2);
  }
}

//Reads HEX up to '.' character and returns an ASCII text String----------------
String lora_commands::readHex2Str(String hexstr)
{
  String newString = "";
  int length = hexstr.length()/2;
  char buf[2];
  int ASCI_dec=0;
  for(int i =0; i <length*2; i+=2)
  {
    buf[0] =hexstr.charAt(i);
    buf[1] =hexstr.charAt(i+1);
    ASCI_dec = int(strtol(buf, NULL, 16));
    buf[0]=ASCI_dec;
    if(MODE==VERBOSE_MODE)
    {
      Serial.print("ASCII: ");
      Serial.print(ASCI_dec);
      Serial.print(" ");
      Serial.println(buf[0]);
    }
    newString+=buf[0];
    if(ASCI_dec==46) break;// End of String
  }
  return newString;
}



//Read serial input and return as String ---------------------------------------
String lora_commands::readSerial2Str()
{
  String response ="";

  while (LoRa_Ser.available())
  {
    response = LoRa_Ser.readString();
    delay(5);
  }
  return response;
}
////////////////////////////////////////////////////////////////////////////////
//                    T E S T I N G   C O M M A N D S                         //
////////////////////////////////////////////////////////////////////////////////
void lora_commands::packetBursting()
{
  //LoRa_Ser.flush();
  //if(MODE) Serial.print("TX>>");
  printString("radio tx 54321");
  printString("\r\n");
}

void lora_commands::newLocation()
{
  //LoRa_Ser.flush();
  //if(MODE) Serial.print("TX>>");
  printString("radio tx 999");
  printString("\r\n");
}
////////////////////////////////////////////////////////////////////////////////
//                    R O U N D   R O B I N   P O L L I N G                   //
////////////////////////////////////////////////////////////////////////////////
void lora_commands::robin_getDataAsGateway()
{
  boolean checks = true;
  //ID Data.
  int N = sizeof(robinIDs);

  if(MODE)
  {
    Serial.print("RobinIDs:");
    for(int x=0; x<N; x++) Serial.print(" '"+String(robinIDs[x])+"'");
    Serial.println();
    Serial.println("N: "+String(N));
  }
  for(int i=0; i <N; i++)
  {
    int theStart = now();
    String data = "TD "+String(robinIDs[i])+" "+String(myLevel)+" "
    +String(myID)+".";
    Serial.println("Sending command for TX "+data);
    printString("radio tx ");
    printHEX(data);
    printString("\r\n");
    delay(150);

    //Wait for radio_tx_ok response
    while((!LoRa_Ser.available()) && (checks==true))//Wait for response that data sent
    {
      if(now()<theStart+_time_out_) delay(50);
      else
      {
        if(MODE) Serial.println("[TIMEOUT] Failed to send TD command");
        checks = false;
      }
    }
    if(checks)//Check if there was no timeout.
    {
      if(strContains(readSerial2Str(),"radio_tx_ok",0))
      {
        if(MODE) Serial.println("Get node"+String(robinIDs[i]));
      }
      else checks = false;
    }

    //Wait for returned data
    while((!LoRa_Ser.available()) && checks==true)
    {
      if(now()<theStart+_time_out_) delay(50);
      else
      {
        if(MODE)
        {
          Serial.println("[TIMEOUT] No response from node"+String(robinIDs[i]));
        }
        checks = false;
      }
    }
    if(checks)//Check if there was no timeout.
    {
      String response = readSerial2Str();
      if(strContains(response, "DM",0))
      {
        //Command has been sent
        if(MODE) Serial.println("Request data from node"+String(robinIDs[i]));
      }
      else checks = false;
    }
  }
}
void lora_commands::robin_setTimeToSend(int temp)
{
  if(MODE) Serial.println("[Node"+String(myID)+"] My turn to send!>>");
  printString("radio tx ");
  printHEX("DM "+String(myLevel)+" "+String(myID)+" "
  +String(temp)+".");//Length
  printString("\r\n");
  delay(1000);
  if(strContains(readSerial2Str(),"radio_tx_ok",0))
  {
    if(MODE) Serial.println("s e n t...>>");
  }
}
void lora_commands::robin_waitForMyCMD(int temp)
{
  boolean checks = true;
  printString("radio rx 0");
  printString("\r\n");
  delay(200);
  String hex_response = readSerial2Str();//nothing
  String response = "";
  int theStart = now();
  if(MODE) Serial.println("Waiting for data. "+hex_response);

  //Wait for a command until timeout or data is received
  while((!LoRa_Ser.available()) && (checks==true))//
  {
    if(now()<theStart+_time_out_) delay(50);
    else
    {
      if(MODE) Serial.println("[ERROR] Timeout");
      checks=false;
    }
  }
  if(checks)//If timeout has not been reached, data is ready
  {
    //Save data from input buffer
    hex_response = readSerial2Str();
    Serial.println("I received this:"+hex_response);
    int start = hex_response.indexOf("rx")+4;
    hex_response.remove(0,start);


    if(MODE) Serial.print("The HEX ["+hex_response);
    response = readHex2Str(hex_response);
    checks = strContains(response,"TD",0);
    Serial.println("] means: "+response);

    if(checks)//If command is a "Transfer Data" type command
    {
      int destID = (String(response.charAt(3))).toInt();
      //int destID = x;
      Serial.println("It is intended for node"+String(destID));
      //If command is intended for this specific node [myID]
      if(destID==myID) robin_setTimeToSend(temp);//robin_setTimeToSend(22);
    }
  }
}
////////////////////////////////////////////////////////////////////////////////
//        P 2 P    N E T W O R K I N G   + + + S E L F  C O N F I G + + +     //
////////////////////////////////////////////////////////////////////////////////
boolean lora_commands::p2p_isTimeToTransmit()
{
  boolean timeToSend = false;
  int myTimeToSend = zero_time + 10*int(myLevel) + int(myID);
  int now_time=now();
  if((now_time >= myTimeToSend) && (now_time < myTimeToSend+10))
  {
    if(!p2p_isMyLevelSet()) if(MODE) Serial.println("[Error] myLevel not set, can't transmit");
    else{
      timeToSend = true;
      //recentTransmission_flag = true;
    }
  }
  return timeToSend;
}

boolean lora_commands::p2p_isMyLevelSet()
{
  boolean checks=true;
  if(myLevel == _noLevel_) checks=false;
  return checks;
}

void lora_commands::p2p_sendSM()
{
  if(MODE) Serial.println("[Level"+String(myID)+" ID"+String(myID)+"] Sending>>");
  printString("radio tx ");
  printHEX("SM "+String(myID)+" "+String(myLevel)+".");//Length
  printString("\r\n");
  delay(1000);
  if(strContains(readSerial2Str(),"radio_tx_ok",0))
  {
    if(MODE) Serial.println("s e n t...>>");
  }
}

//
void lora_commands::p2p_getSM()
{
  boolean checks = true;
  int count = 0;
  while((!LoRa_Ser.available()) && (count<4))//
  {

    delay(50);
    count++;
    if(count==4) checks = false;
  }
  if(checks)
  {
    //Save data from input buffer
    delay(50);
    String hex_response = readSerial2Str();
    if(MODE) Serial.println("I received this:"+hex_response);
    int start = hex_response.indexOf("rx")+4;
    hex_response.remove(0,start);

    if(MODE) Serial.print("The HEX ["+hex_response);
    String response = readHex2Str(hex_response);
    checks = strContains(response,"SM",0);//Is it self-configuration data
    Serial.println("] means: "+response);

    if(checks)//If command is a "Transfer Data" type command
    {
      int sID = (String(response.charAt(3))).toInt();
      int sLevel = (String(response.charAt(5))).toInt();
      /*
      *CHECK SNR
      *double getSNR()
      */
      //if(getSNR()>4.2) //SNR is good enough
      if(true)
      {
        if(sLevel< int(myLevel))//1 Level closer than me
        {
          myLevel = sLevel+1;
        }
      }
    }
  }
}
double lora_commands::p2p_getSNR()
{/*
  printString("radio get snr");
  printString("\r\n");
  delay(100);
  String response = readSerial2Str();
  int start = response.indexOf("rx")+4;
  response.remove(0,start);
  return SNR;*/
}

////////////////////////////////////////////////////////////////////////////////
//        P 2 P    N E T W O R K I N G   + + + D A T A    M O D E + + +       //
////////////////////////////////////////////////////////////////////////////////
