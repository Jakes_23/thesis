#include <gsm_commands.h>


//Defines the amount of output to PC via Serial
#define MODE DEBUGGING_MODE
int iSTAT = 0;//Status of the modem

//Instantiate the serial connection with Modem
SoftwareSerial mySerial(rxPin,txPin); // RX of Arduino, TX of Arduino

gsm_commands::gsm_commands()
{
  //Instantiate pin modes
  pinMode(STATPIN, INPUT);
  pinMode(PWR, OUTPUT);
}

//Wakes up GSM Module -----------------------------------------------------------------------------------
boolean gsm_commands::wakeUp()
{
  if(MODE==VERBOSE_MODE) Serial.println("[GSM module] VERBOSE MODE");
  else if(MODE == DEBUGGING_MODE) Serial.println("[GSM module] DEBUGGING MODE");

  boolean checks=true;//Used to return if errors occurred
  mySerial.begin(9600);//Start the serial communication with modem at 9600baud

  if(isAwake()==false)//If modem is in sleep mode turn it on
  {
    digitalWrite(PWR, HIGH);
    delay(1300);
    digitalWrite(PWR, LOW);
    delay(15);
    mySerial.begin(9600);
    delay(2);
  }
  if(isAwake()==false)//If modem has not woken up return error
    {
      checks=false;
      if(MODE) Serial.println("[Error] GSM wake-up failed");
    }
    else//SUCCESS
    {
      //Send command for AUTO baud detection
      issueCommand("AT");
      delay(2500);//>2s delay prevents strange characters being received after auto baud
      mySerial.flush();

      if(MODE) Serial.println("GSM Awake! >>");//Module is awake & ready to receive data
      //issueCommand("AT+COPS=0");
      //delay(600);
    }
  return checks;
}

//Puts GSM in sleep mode -------------------------------------------------------------------------------
void gsm_commands::sleep()
{
  if(isAwake()==true)//If awake go to sleep mode
  {
    issueCommand("AT+QPOWD=1");
    if(MODE) Serial.println("GSM Shutting down >>");
  }
}

//Read serial input and output it directly to PC ---------------------------------------------------------------
void gsm_commands::readSerial2PC()
{
  while (mySerial.available())
  {
    Serial.write(mySerial.read());
    delay(7);
  }
}

//Read serial input and return as String -------------------------------------------------------------
String gsm_commands::readSerial2Str()
{
  String response ="";

  while (mySerial.available())
  {
    response = mySerial.readString();
    delay(7);
  }
  return response;
}

//Send command terminating with <CR> to GSM module --------------------------------------------------
void gsm_commands::issueCommand(char* command)
{
  mySerial.print(command);
  mySerial.print("\r");
  delay(12);
}

//Checks status of network registration-------------------------------------------------------------
boolean gsm_commands::isRegistered()
{
  boolean checks = true;
  String A = readSerial2Str();
  delay(10);
  issueCommand("AT+CREG?");
  delay(500);
  //A = readSerial2Str();
  //delay(100);
  //Serial.println("---");
  //Serial.println(A);
  //Serial.println("---");
  String response = readSerial2Str();
  Serial.print("Response: ");
  Serial.println(response);
  if(MODE==VERBOSE_MODE)
  {
    Serial.println(response);
    delay(500);
    Serial.println("Current network:");
    issueCommand("AT+COPS?");
    delay(600);
    readSerial2PC();
    delay(100);
    Serial.println("Available networks:");
    issueCommand("AT+CPOL?");
    delay(1500);
    readSerial2PC();
    delay(5000);
    readSerial2PC();
    delay(50);
    issueCommand("AT+CSQ");
    delay(500);
    readSerial2PC();
  }
  else if(strContains(response,"0,5",0))//SIM registration NOT ready
  {
    if(MODE)
    {
      Serial.println("Roaming...");
      delay(50);
      issueCommand("AT+CSQ");
      delay(500);
      readSerial2PC();
    }
  }
  else if(strContains(response,"0,2",0))//SIM registration NOT ready
  {
    if(MODE)
    {
      Serial.println("[Error] Still searching for network...");
    }
    checks = false;
  }
  else if(strContains(response,"0,3",0))//SIM registration NOT ready
  {
    if(MODE)
    {
      Serial.println("[Error] Registration denied");
    }
    checks = false;
  }
  else if(strContains(response,"0,1",0))//SIM registration NOT ready
  {
    if(MODE)
    {
      Serial.println("SIM registered >>");
      delay(50);
      issueCommand("AT+CSQ");
      delay(500);
      readSerial2PC();
    }
  }
  else //SIM registration SUCCESS
  {
    if(MODE)
    {
      Serial.println("[Error] Other registration error.");
    }
    checks = false;
  }
  delay(100);
  return checks;//returns success as boolean
}

//Checks if the GSM module is awake-----------------------------------------------------------
boolean gsm_commands::isAwake()
{
  boolean awake = false;
  iSTAT = digitalRead(STATPIN);
  if(iSTAT==1)
  {
    awake = true;
  }
  return awake;
}

//register for GPRS------------------------------------------------------------------------------
boolean gsm_commands::setGPRS_on()
{
  boolean checks = true;
  issueCommand("AT+CGATT=1");
  delay(300);
  if(MODE==VERBOSE_MODE) readSerial2PC();
  else{
    if(!strContains(readSerial2Str(),"OK",0))
    {
      if(MODE) Serial.println("[Error] GPRS attach failed.");
      checks = false;
    }
  else if(MODE) Serial.println("GPRS activated >>");
  }
  delay(100);
  return checks;
}

//Tests HTTP capability--------------------------------------------------------------------------
void gsm_commands::ping(String website)
{
  //String command = "AT+QHTTPURL=13,10" + website;
  //issueCommand("AT+QPING=13,10");
  issueCommand("AT+QPING=\"216.58.223.35\"");//http://www.google.co.za
  delay(1000);
  readSerial2PC();
  delay(1000);
  readSerial2PC();
  Serial.println();
}

boolean gsm_commands::strContains(String response, String subString, int startI)
{
  boolean contains = false;
  int n = subString.length();
  int matched = 0;
  for (int i = startI; i< response.length(); i++)
  {
    if(matched<n)
    {
      if(response.charAt(i)==subString.charAt(matched))
      {
        matched+=1;
      }
    }
  }
  if(matched == n) contains = true;
  return contains;
}




void gsm_commands::TCP_send(float data_value)
{
  /*
  name:       tempSense
  url:        api.sense-os.nl/sensors/2321167
  session ID: ea53880b102e77e07dc0cce25b3e1ccd
  */
  StaticJsonBuffer<128> jsonBuffer;
  JsonObject &encodedJSON = jsonBuffer.createObject();
  JsonArray &array = encodedJSON.createNestedArray("data");
  JsonObject &inside = array.createNestedObject();
  inside["value"] = data_value;
  char buffer[28];
  encodedJSON.printTo(buffer, sizeof(buffer));

  String container="";
  if(MODE==VERBOSE_MODE)
  {
    Serial.print("JSON data: ");
    Serial.println(buffer);
  }

  //Connect to TCP server
  issueCommand("AT+QIOPEN=\"TCP\",\"64.90.48.15\",80"); //AT+QIOPEN=<mode>,<IP/Domain>,<port>
  int temp=0;
  String myS = "";
  for (int y=0; y< 60; y++)
  {
    //Serial.println(RRR);
    if(!strContains(readSerial2Str(),"CONNECT",0))
    {
      delay(1000);
      if(MODE) Serial.println(y);
      //temp=y;
    }
    else
    {
      temp=y;
      y=60;
      if(MODE)
      {
        Serial.println("CONNECT");
        Serial.println("Sending "+String(buffer));
      }
      mySerial.print("AT+QISEND\r");//number of characters + 2
      for (int x=temp; x< 60; x++)
      {
        myS = readSerial2Str();
        Serial.println(myS);
        if(!strContains(myS,">",0))
        {
          delay(1000);
          Serial.println(x);
          if(x==59) if(MODE) Serial.println("[ERROR] No '>' received");
          //temp=y;
        }
        else{
          x=60;
          mySerial.print("POST /post.php HTTP/1.1\r\n");//25
          delay(50);
          readSerial2PC();
          mySerial.print("HOST: posttestserver.com\r\n");//26 (50)
          delay(50);
          readSerial2PC();
          mySerial.print("Accept: application/json\"\r\n");//13
          delay(50);
          readSerial2PC();
          mySerial.print("User-Agent: QUECTEL_MODULE2\r\n");//29
          delay(50);
          readSerial2PC();
          mySerial.print("Content-Length: 28\r\n");//19
          delay(50);
          readSerial2PC();
          mySerial.print("\r\n");//2
          readSerial2PC();
          mySerial.print(buffer);
          mySerial.print("\r");
          delay(50);
          delay(50);
          mySerial.print("\x1A\r");// CTRL+Z
          readSerial2PC();
          delay(100);
          readSerial2PC();
          delay(100);
          readSerial2PC();
          delay(100);
          readSerial2PC();
        }
      }
    }
  }
  delay(1000);
  readSerial2PC();
  mySerial.print("+++");
  delay(1000);
  mySerial.print("AT+QCLOSE\r");
}

/*
//Posts data to server in JSON format with indicated HTML headers -----------------------------------
void gsm_commands::postData(float data_value) //Must be in format value: ""
{
  StaticJsonBuffer<128> jsonBuffer;
  JsonObject &encodedJSON = jsonBuffer.createObject();
  JsonArray &array = encodedJSON.createNestedArray("data");
  JsonObject &inside = array.createNestedObject();
  inside["value"] = data_value;
  char buffer[28];
  encodedJSON.printTo(buffer, sizeof(buffer));

  String container="";
  int temp =0;

  if(MODE==VERBOSE_MODE)
  {
    Serial.print("JSON data: ");
    Serial.println(buffer);
  }

  //ENTER HTTP MODE
  //Connect to sense-IoT server

}
*/

/*
boolean gsm_commands::connect2SensorServer()
{
  boolean checks = true;

  issueCommand("AT+QHTTPURL=34,1");

  delay(150);
  //issueCommand("http://api.sense-os.nl/sensors/2321167/data.json");//48
  //issueCommand("http://httpbin.org/post");
  issueCommand("http://posttestserver.com/post.php");//https://posttestserver.com/post.php //34/58

  //issueCommand("http://www.test.sirkits.co.za/data.txt");
  delay(500);
  if(MODE==VERBOSE_MODE) readSerial2PC();
  else if(!strContains(readSerial2Str(),"OK",0))
  {
    if(MODE==DEBUGGING_MODE) Serial.println("[Error] Sense-IoT Connection Failed");
    checks = false;
  }
  else if(MODE==DEBUGGING_MODE) Serial.println("Sense-IoT Connection Established >>");

  return checks;
}
*/


/*
boolean gsm_commands::setHeaders(int request, int response)
{
  boolean checks = true;
  char command[29];
  sprintf(command, "AT+QHTTPCFG=\"requestheader\",%d", request);
  issueCommand(command); //Setting: Want to send customized HTTP request headers
  delay(250);
  if(MODE==VERBOSE_MODE) readSerial2PC();
  else if(!strContains(readSerial2Str(),"OK",0)) checks=false;

  delay(10);
  if((checks) && (response != 0))
  {
    sprintf(command, "AT+QHTTPCFG=\"responseheader\",%d", response);
    issueCommand(command); //Setting: Want to send customized HTTP response headers
    delay(250);
    if(MODE==VERBOSE_MODE) readSerial2PC();
    else if(!strContains(readSerial2Str(),"OK",0)) checks=false;
  }

  if(MODE==DEBUGGING_MODE)
  {
    if(checks) Serial.println("Headers Set >>");
    else Serial.println("Setting Headers Failed");
  }
  return checks;
}
*/



/*
boolean gsm_commands::registerOnPreferredNetwork()
{
  boolean checks = false;
  int start = now();
  int timeout=20;
  String response="";
  issueCommand("AT+COPS=1,2,\"65507\""); //Select Cell C
  delay(1500);
  if(MODE)
  {
    while(now() < start + timeout)
    {
      delay(750);
      response= readSerial2Str();
      //if((response != NULL) && (response != "")) Serial.println(response);
      if(strContains(response,"OK",0))
      {
        Serial.println("OK");
        checks=true;
        break;
      }
      else if(now() > start + timeout) Serial.println("[TIMEOUT] Could not register on CellC");
    }
  }
  return checks;
}
*/

/*USELESS CODE
*
*if(connect2SensorServer()==true)
{
  AT+QHTTPCFG="requestheader",1
  AT+QHTTPPOST=77
  GET path HTTP/1.1
  User-Agent: Fiddler
  Serial.println("Setting header...>>");
  delay(50);
  issueCommand("AT+QHTTPCFG=\"Accept\",\"application/json\"");
  delay(1500);
  if(!strContains(readSerial2Str(),"OK",0)) Serial.println("[ERROR] Failed to send header1");
  else Serial.println("JSON Header ready >>");


  //issueCommand("http://posttestserver.com/post.php");
  issueCommand("AT+QHTTPPOST=5,77,7");
  GET path HTTP/1.1
  User-Agent: Fiddler
  delay(20);
  setHeaders(1,0); //setHeaders(int request, int response)
  issueCommand("AT+QHTTPPOST=39,60,7");//163
  //issueCommand("AT+QHTTPPOST=0,60,7");
  //delay(1500);
  String RRR = readSerial2Str();
  Serial.print("Before Headers: ");
  Serial.println(RRR);
  //SEND REQUEST HEADER

  for (int y=1; y< 60; y++)
  {
    RRR = readSerial2Str();
    //Serial.println(RRR);
    if(!strContains(RRR,"CONNECT",0))
    {
      delay(1000);
      Serial.println(y);
      temp=y;
    }
    else
    {
      //mySerial.print("POST/post.php HTTP/1.1\r\n");//25
      //mySerial.print("HOST: posttestserver.com\r\n");//26 (50)
      //mySerial.print("Accept: **\r\n");//13
      //mySerial.print("User-Agent: QUECTEL_MODULE2\r\n");//29
      mySerial.print("key: \"User-Agent\" = \"QUECTEL_MODULE2\"\r\n");//29
      //mySerial.print("Connection: Keep-Alive\r\n");//24
      //Content-Type: application/x-www-form-urlencoded<CR><LF>
      //mySerial.print("Content-Type: application/x-www-form-urlencoded\r\n");//49
      //mySerial.print("Content-Length: 0\r\n");//19
      mySerial.print("\r\n");//2

      if(MODE) Serial.println("Ready. Sending... >>>>");
      //mySerial.print(command);
      //mySerial.print("\r");
      //URL: http://posttestserver.com/post.php
      //mySerial.print("h\r");
      temp=y;
      y=60;
    }
  }

  for (int x=temp; x< 60; x++)
  {
    container = readSerial2Str();
    Serial.println(container);
    if(!strContains(container,"OK",0))
    {
      delay(1000);
      Serial.println(x);
      if(x==59) Serial.println("[ERROR] Sending failed.");
    }
    else
    {
      x=60;
      if(MODE) Serial.println("DATA SENT! >>");
    }
  }
  readSerial2PC();
  delay(1500);
  issueCommand("AT+QHTTPREAD=5");
  for(int z = 0; z<5; z++)
  {
    delay(1000);
    Serial.println(z);
    readSerial2PC();
  }
}*/
