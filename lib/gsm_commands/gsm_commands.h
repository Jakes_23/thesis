    #ifndef gsm_commands_H
    #define gsm_commands_H

    #include <SoftwareSerial.h>
    #include <ArduinoJson.h>
    #include <main.h>

    //Pin Declaration
    #define rxPin 7
    #define txPin 8
    #define STATPIN 6   //Modem Status Pin
    #define PWR 5



    class gsm_commands{
    public:
      gsm_commands();

      //shut down sequence
      void sleep();
      boolean wakeUp(); //Wakes up module and returns success

      //initialisation and checks
			boolean isAwake();
      boolean isRegistered();
      boolean setGPRS_on();
      boolean strContains(String response, String subString, int startI);
      void ping(String website);
      boolean registerOnPreferredNetwork();

      //Talk to GSM
      void readSerial2PC();
      String readSerial2Str();
      void issueCommand(char* msg);

      //Post JSON data
      //void postData(float data_value);
      //boolean connect2SensorServer();
      //void sendTCPdata();
      //boolean setHeaders(int request, int response);
      void TCP_send(float data_value);





    };

    #endif
