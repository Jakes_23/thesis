//#include <main.h>
#include <GSM_commands.h>
#include <sensor_commands.h>
#include <lora_commands.h>
#include <time.h>

void initialiseModem();
boolean initialiseLora();
void initialiseTest();
boolean retryRegistration();

gsm_commands myGSM; //Instantiate my GSM library
sensor_commands tempSensor; //Instantiate my temperature sensor
lora_commands lora;

void setup()//Activate modem, do commands and switch off
{
  //Start serial communications with PC


  Serial.begin(9600);
  delay(2500);
  //Make sure the extra LED on Arduino is off (Used to show errors)
  pinMode(13, OUTPUT);
  digitalWrite(13, LOW);
  /*
  Serial.println("Testing LoRa functionality...>>");




  //
  initialiseLora();
  Serial.println("String to be transmitted: "+lora.string2HEXstr("TD 2 1 0."));
  lora.robin_waitForMyCMD(23);

  lora.robin_getDataAsGateway();
  lora.sleep();
  */
  //Serial.println(lora.string2HEXstr("TD"));
  //

  //lora.sleep10();

  //Serial.println("Received: 0x53312035352032332e");
  //Serial.println("Converting back to ASCII...>>");
  //erial.print("Result: ");
  //Serial.println(lora.readHex2Str("53312035352032332e "));


  Serial.println("Testing GSM functionality...>>");
  //initialiseModem();
  myGSM.wakeUp();

  //myGSM.ping("http://www.google.co.za");
  myGSM.TCP_send(tempSensor.getTemperature());
  //myGSM.postData(tempSensor.getTemperature()); //Posts data to the cloud
  //myGSM.sendTCPdata();
  delay(3000);
  //myGSM.sleep();
  //delay(2500);

}

void loop()
{
  delay(850);//do nothing
  Serial.println();
}

void initialiseModem()
{
  /*
   * See the file: GSM_commands.cpp for details of these functions
*/
  if(!myGSM.wakeUp()) digitalWrite(13, HIGH);
  delay(2500);
  //if(!myGSM.registerOnPreferredNetwork()) digitalWrite(13, HIGH);
  myGSM.issueCommand("AT+COPS=0");
  delay(2500);
  //myGSM.readSerial2PC();
  myGSM.readSerial2Str();
  int n =0;
  while(!retryRegistration() && (n<5))
  {
    n++;
    if(n<3) if(MODE) Serial.println("Retrying...");
    /*else if(n==3)
    {
      if(MODE) Serial.println("Trying automatic operator");
      myGSM.issueCommand("AT+COPS=0");
      delay(1500);
      myGSM.readSerial2PC();
    }*/
    else if(n>3)
    {
      Serial.println("[ERROR] Registration impossible.");
      break;
    }
  }
}

boolean retryRegistration()
{
  boolean checks = true;
  if(!myGSM.isRegistered())
  {
    checks = false;
    delay(12000);
  }
  else if(!myGSM.setGPRS_on()) digitalWrite(13, HIGH);
  return checks;
}
boolean initialiseLora()
{
  boolean checks = false;
  lora.wakeUp();
  if(!lora.makeReady()) digitalWrite(13, HIGH);
  else checks=true;
  return checks;
}


void initialiseTest()
{
  int start = now();
  int tNow=0;
  int count =0;
  while(count<60+2)
  {
    tNow=now()-start;
    if(count==tNow)
    {
      lora.readSerial2PC();
      Serial.println(count);
      if(count==0)
      {
        Serial.println("Transmitting Location");
        lora.newLocation();
        delay(150);
      }
      if(count>=2)
      {
        digitalWrite(13, HIGH);
        lora.packetBursting();
        delay(100);
        digitalWrite(13, LOW);
      }
      count++;
    }
  }
  Serial.print("Sent: ");
  Serial.print(count-2);
  Serial.println("packets");
}
